var num_array = [];
for(var i=0; i<10; i++){
	num_array[i] = 2*i;
}

var i1 = iterative_binary_search(num_array, 7, num_array[0], num_array[num_array.length-1]);
var i2 = iterative_binary_search(num_array, 12, num_array[0], num_array[num_array.length-1]);
var r1 = recusive_binary_search(num_array, 19, num_array[0], num_array[num_array.length-1]);
var r2 = recusive_binary_search(num_array, 12, num_array[0], num_array[num_array.length-1]);
console.log("i1: ", i1, "i2: ", i2);
console.log("r1: ", r1, "r2: ", r2);


function iterative_binary_search (num_array, value, low, high) {
	var mid;
	while(low <= high){
		mid = Math.floor((low+high)/2);
		if(value == num_array[mid]){
			return mid;
		}else if(value > num_array[mid]){
			low = mid + 1;
		}else{
			high = mid -1;
		}
	}
	return -1;
}

function recusive_binary_search (num_array, value, low, high) {
	if(low > high){
		return -1;
	}
	var mid = Math.floor((low + high) / 2);
	if(value == num_array[mid]){
		return mid;
	}else if(value > num_array[mid]){
		return recusive_binary_search(num_array, value, mid+1, high);
	}else{
		return recusive_binary_search(num_array, value, low, mid-1);
	}
}