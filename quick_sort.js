var A = [];
for(var i=0; i<10; i++){
	A[i+1] = 10 - i;
}

// quicksort(A, 1, 10);
randomized_quicksort(A, 1, 10);

for(var i = 1; i<= 10; i++) {
	console.log(A[i]);
}

function quicksort (A, p, r) {
	if(p<r) {
		var q = partition(A, p, r);
		quicksort(A, p, q-1);
		quicksort(A, q+1, r);
	}
}

function randomized_quicksort (A, p, r) {
	if(p<r) {
		var q = randomized_patition(A, p, r);
		randomized_quicksort(A, p, q-1);
		randomized_quicksort(A, q+1, r);
	}
}

/*
	partition
*/
function partition (A, p, r) {
	var x = A[r];
	var i = p-1;
	var j = p;
	for (j = p; j <= r-1; j++) {
		if(A[j] <= x) {
			i++;
			var temp = A[i];
			A[i] = A[j];
			A[j] = temp;
		}
	}
	var temp = A[i+1];
	A[i+1] = A[r];
	A[r] = temp;
	return i+1;
}


/*
	randomized partition
*/
function randomized_patition (A, p, r) {
	var i = p + Math.floor(Math.random()*(r - p));
	var temp = A[r];
	A[r] = A[i];
	A[i] = temp;
	return partition(A, p, r);
}